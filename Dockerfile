FROM node:18-slim
MAINTAINER Gudditi
WORKDIR /app
COPY . .
RUN npm ci 
EXPOSE 3000
ENTRYPOINT ['node','app.js']